.syntax unified

.include "efm32gg.s"

/////////////////////////////////////////////////////////////////////////////
//
// Exception vector table
// This table contains addresses for all exception handlers
//
/////////////////////////////////////////////////////////////////////////////

.section .vectors

.long   stack_top              			/* Top of Stack                 *///0x00
.long   _reset              	   		/* Reset Handler                *///0x04
.long   system_error_handler	        /* NMI Handler                  *///0x08
.long   system_error_handler    	    /* Hard Fault Handler           *///0x0c
.long   system_error_handler        	/* MPU Fault Handler            *///0x10
.long   system_error_handler           	/* Bus Fault Handler            *///0x14
.long   system_error_handler           	/* Usage Fault Handler          *///0x18
.long   system_error_handler           	/* Reserved                     *///0x1c
.long   system_error_handler         	/* Reserved                     *///0x20
.long   system_error_handler           	/* Reserved                     *///0x24
.long   system_error_handler           	/* Reserved                     *///0x28
.long   system_error_handler            /* SVCall Handler               *///0x2c
.long   system_error_handler          	/* Debug Monitor Handler        *///0x30
.long   system_error_handler           	/* Reserved                     *///0x34
.long   system_error_handler            /* PendSV Handler               *///0x38
.long   system_error_handler           	/* SysTick Handler              *///0x3c

/* External Interrupts */
.long   dummy_handler			/* DMA-Direct Memory Acces 		*///0x40
.long   gpio_handler            /* GPIO even handler 			*///0x44
.long   dummy_handler			/* Timer0						*///0x48
.long   dummy_handler			/* USART0_RX					*///0x4c
.long   dummy_handler			/* USART0_TX					*///0x50
.long   dummy_handler			/* USB							*///0x54
.long   dummy_handler			/* ACMP0/ACMP1					*///0x58
.long   dummy_handler			/* ADC0							*///0x5c
.long   dummy_handler			/* DAC0							*///0x60
.long   dummy_handler			/* I2C0							*///0x64
.long   dummy_handler			/* I2C1							*///0x68
.long   gpio_handler            /* GPIO odd handler 			*///0x6c
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler			/*CMU-Clock Management Unit		*/
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler			/*EMU-Energy Management Unit	*/

.section .text

iser0_addr:
	.long ISER0

/////////////////////////////////////////////////////////////////////////////
//
// Reset handler
// The CPU will start executing here after a reset
//
/////////////////////////////////////////////////////////////////////////////

		.globl  _reset
		.type   _reset, %function	
		.thumb_func	
_reset:

	b _start
	
	
	.thumb_func
_start:

		//Load base registers
		ldr r1, = GPIO_BASE
		ldr r2, = GPIO_PA_BASE	//LED	(8-15)
		ldr r3, = GPIO_PC_BASE	//BUTTON (0-7)
		ldr r8, = EMU_BASE

		//Enable GPIO clock
		ldr r4, =CMU_BASE
		ldr r5, [r4, #CMU_HFPERCLKEN0_GPIO]
		mov r6, #1
		lsl r6, r6, #CMU_HFPERCLKEN0_GPIO
		orr r5, r5, r6
		str r5, [r4, #CMU_HFPERCLKEN0]


		//Set drive mode on output (LEDS)
		mov r4, #0x2
		str r4, [r2, #GPIO_CTRL]

		//Set Port A to output (LEDS)
		mov r4, #0x55555555
		str r4, [r2, #GPIO_MODEH]
		
		//init led off
		mov r4, #0xff00
		str r4, [r2, #GPIO_DOUT]
	
		//Set Port C to input (BUTTONS)
		mov r4, #0x33333333
		str r4, [r3, #GPIO_MODEL]
		
		//Enable internal pullup PORT C (BUTTONS)
		mov r4, #0xff
		str r4, [r3, #GPIO_DOUT]

		//Enable external interrupt on PORT C
		mov r4, #0x22222222
		str r4, [r1, #GPIO_EXTIPSELL]

		//Enable falling/rising edge interrupt
		mov r4, #0xff
		str r4, [r1, #GPIO_EXTIFALL] 
		str r4, [r1, #GPIO_EXTIRISE]
		str r4, [r1, #GPIO_IEN]

		
		//Enable interrupt handling
		ldr r4, =#0x802 // 1<< GPIO_ODD_HANDLER | 1 << GPIO_EVEN_HANDLER
		ldr r5,	iser0_addr
		str r4, [r5]
		
		
		//Configure energy mode 2	
		ldr r4, =SCR
		mov r5, #0x06 
		str r5, [r4] //DEEPSLEEP
		
		wfi
		b main_loop
		
		

			
	.thumb_func
main_loop:
	
	//wfi

	b main_loop		
		
	
/////////////////////////////////////////////////////////////////////////////
	.thumb_func
energy_mode4:

		//write sequence to enter EM4
		ldr r4, =EMU_BASE
		
		mov r5, #0x00
		str r5, [r4, #EMU_CTRL]
		
		ldr r5, =0b1000 //2
		ldr r6, =0b1100 //3		
		str r5, [r4, #EMU_CTRL]
		str r6, [r4, #EMU_CTRL]
		str r5, [r4, #EMU_CTRL]
		str r6, [r4, #EMU_CTRL]
		str r5, [r4, #EMU_CTRL]
		str r6, [r4, #EMU_CTRL]	
		str r5, [r4, #EMU_CTRL]	
		str r6, [r4, #EMU_CTRL]
		str r5, [r4, #EMU_CTRL]
		
		wfi
		
		b main_loop	

/////////////////////////////////////////////////////////////////////////////
//
// GPIO handler
// The CPU will jump here when there is a GPIO interrupt
//
/////////////////////////////////////////////////////////////////////////////

		.thumb_func
gpio_handler:

		//Clear interrupt Port C
		ldr r4, [r1, #GPIO_IF]
		str r4, [r1, #GPIO_IFC]


		ldr r4, [r3, #GPIO_DIN]
		lsl r4, r4, 8
		str r4, [r2, #GPIO_DOUT]

		bx lr
		

/////////////////////////////////////////////////////////////////////////////
	.thumb_func
dummy_handler:
		b .
		

/////////////////////////////////////////////////////////////////////////////
	.thumb_func
system_error_handler:
		b . 






