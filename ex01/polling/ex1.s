.syntax unified

.include "efm32gg.s"

/////////////////////////////////////////////////////////////////////////////
//
// Exception vector table
// This table contains addresses for all exception handlers
//
/////////////////////////////////////////////////////////////////////////////

.section .vectors

.long   stack_top               	/* Top of Stack                 *///0x00
.long   _reset                  /* Reset Handler                *///0x04
.long   dummy_handler           /* NMI Handler                  *///0x08
.long   dummy_handler           /* Hard Fault Handler           *///0x0c
.long   dummy_handler           /* MPU Fault Handler            *///0x10
.long   dummy_handler           /* Bus Fault Handler            *///0x14
.long   dummy_handler           /* Usage Fault Handler          *///0x18
.long   dummy_handler           /* Reserved                     *///0x1c
.long   dummy_handler           /* Reserved                     *///0x20
.long   dummy_handler           /* Reserved                     *///0x24
.long   dummy_handler           /* Reserved                     *///0x28
.long   dummy_handler           /* SVCall Handler               *///0x2c
.long   dummy_handler           /* Debug Monitor Handler        *///0x30
.long   dummy_handler           /* Reserved                     *///0x34
.long   dummy_handler           /* PendSV Handler               *///0x38
.long   dummy_handler           /* SysTick Handler              *///0x3c

/* External Interrupts */
.long   dummy_handler			/* DMA-Direct Memory Acces 		*///0x40
.long   gpio_handler            /* GPIO even handler 			*///0x44
.long   dummy_handler			/* Timer0						*///0x48
.long   dummy_handler			/* USART0_RX					*///0x4c
.long   dummy_handler			/* USART0_TX					*///0x50
.long   dummy_handler			/* USB							*///0x54
.long   dummy_handler			/* ACMP0/ACMP1					*///0x58
.long   dummy_handler			/* ADC0							*///0x5c
.long   dummy_handler			/* DAC0							*///0x60
.long   dummy_handler			/* I2C0							*///0x64
.long   dummy_handler			/* I2C1							*///0x68
.long   gpio_handler            /* GPIO odd handler 			*///0x6c
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler			/*CMU-Clock Management Unit		*/
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler
.long   dummy_handler			/*EMU-Energy Management Unit	*/

.section .text
// Exception vector



/////////////////////////////////////////////////////////////////////////////
//
// Reset handler
// The CPU will start executing here after a reset
//
/////////////////////////////////////////////////////////////////////////////

		.globl  _reset
		.type   _reset, %function	
		.thumb_func	
_reset:
		//Load base registers
		ldr r1, = GPIO_BASE
		ldr r2, = GPIO_PA_BASE	//LED	
		ldr r3, = GPIO_PC_BASE	//BUTTON
		
		//Enable GPIO clock
		ldr r4, =CMU_BASE
		mov r5, #1
		lsl r5, r5, #CMU_HFPERCLKEN0_GPIO
		str r5, [r4, #CMU_HFPERCLKEN0]


		//Set drive mode on output (LEDS)
		mov r4, #0x2
		str r4, [r2, #GPIO_CTRL]

		//Set Port A to output (LEDS)
		mov r4, #0x55555555
		str r4, [r2, #GPIO_MODEH]
	
		//Turn LEDS off (active low)
		mov r4, #0xff00
		str r4, [r2, #GPIO_DOUT]
	
		//Set Port C to input (BUTTONS)
		mov r4, #0x33333333
		str r4, [r3, #GPIO_MODEL]
		
		
		//Enable internal pullup PORT C (BUTTONS)
		mov r4, #0xff
		str r4, [r3, #GPIO_DOUT]
		
		b polling
		
///////////////////////////////////////////////////////////////////
//
// Polling loop 
// Make polling loop to check pin status
//
///////////////////////////////////////////////////////////////////
	.thumb_func
polling:
	
	ldr r4, [r3, #GPIO_DIN]
	lsl r4, r4, #8
	str r4, [r2, #GPIO_DOUT]
	
	b polling



/////////////////////////////////////////////////////////////////////////////
//
// GPIO handler
// The CPU will jump here when there is a GPIO interrupt
//
/////////////////////////////////////////////////////////////////////////////

		.thumb_func
gpio_handler:
		b . // do nothing



/////////////////////////////////////////////////////////////////////////////
	.thumb_func
dummy_handler:
		b .  // do nothing




