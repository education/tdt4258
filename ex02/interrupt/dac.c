#include <stdint.h>
#include <stdbool.h>
#include <stdint.h>

#include "efm32gg.h"
#include "dac.h"
#include "gpio.h"


void
setupDAC ()
{
  *CMU_HFPERCLKEN0 |= CMU2_HFPERCLKEN0_DAC0;	//enable clock to DAC
  *DAC0_CTRL |= 0x50010;	// prescale DAC to 437.5KHz and set OUTMODE to pin
  *DAC0_CH0CTRL |= 1;		// enable channel 0 
  *DAC0_CH1CTRL |= 1;		// enable channel 1
}

void
writeDAC (uint16_t data)
{
  *DAC0_CH0DATA = data;
  *DAC0_CH1DATA = data;
};

void
DAC_off (void)
{
  *DAC0_CTRL &= ~0x50010;	// disable prescaler and OUTMODE
  *DAC0_CH0CTRL = 0;		// disable channel 0 
  *DAC0_CH1CTRL = 0;		// disable channel 1
  *CMU_HFPERCLKEN0 &= ~CMU2_HFPERCLKEN0_DAC0;	//disable clock to DAC
};
