#ifndef DAC_H
#define DAC_H

/* enabes clock, channels and prescaler */
void setupDAC (void);

/* writed data to DAC channel1 and 2 */
void writeDAC (uint16_t data);

/* disables clock, channels and prescaler */
void DAC_off ();


#endif
