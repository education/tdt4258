#include <stdint.h>
#include <stdbool.h>

#include "efm32gg.h"

/* function to set up GPIO mode and interrupts*/
void
setupGPIO ()
{
  *CMU_HFPERCLKEN0 |= CMU2_HFPERCLKEN0_GPIO;	// enable GPIO clock

  //LEDS   
  *GPIO_PA_CTRL = 2;		// set high drive strength
  *GPIO_PA_MODEH = 0x55555555;	// set pins A8-15 as output
  *GPIO_PA_DOUT = 0xff00;	// turn off LEDs D4-D8 (LEDs are active low)

  //BUTTONS
  *GPIO_PC_MODEL = 0x33333333;
  *GPIO_PC_DOUT = 0xff;		// set internal pull-up

  //INTERRUPT
  *GPIO_EXTIPSELL = 0x22222222;	// enable interrupt on PORT C, pin 0-7
  *GPIO_EXTIFALL = 0xff;	// enable falling edge interrupt
  *GPIO_EXTIRISE = 0xff;	// enable rising edge interrupt
  *GPIO_IEN = 0xff;		// enable interrupt


}

uint32_t
gpio_read_buttons (void)
{
  return ~(*GPIO_PC_DIN);	// negated due to pull-up on button
}
