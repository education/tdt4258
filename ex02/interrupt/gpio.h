#ifndef GPIO_H
#define GPIO_H


/*
* enable leds and button with interrupt.
*/
void setupGPIO ();

/* read button stade
*  return high for button pressed
*/
uint32_t gpio_read_buttons ();


#endif
