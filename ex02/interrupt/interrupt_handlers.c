#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include "efm32gg.h"
#include "gpio.h"
#include "sounds.h"
#include "notes.h"
#include "dac.h"
#include "timer.h"

static int k = 0;

static uint32_t last_input = 0;


void GPIO_handler ();

int btn_nr = -1;


/* TIMER1 interrupt handler */
void __attribute__ ((interrupt)) TIMER1_IRQHandler ()
{
  *TIMER1_IFC = *TIMER1_IF;	//clear overflow bit

  switch (btn_nr)
    {
    case (1):
      if (play_sound1 () < 0)
	{
	  btn_nr = -1;
	  stop_sound ();
	}
      break;

    case (2):
      if (play_c_scale () < 0)
	{
	  btn_nr = -1;
	  stop_sound ();
	}
      break;

    case (3):
      if (play_sound3 () < 0)
	{
	  btn_nr = -1;
	  stop_sound ();
	}
      break;

    case (4):
      if (play_lisa () < 0)
	{
	  btn_nr = -1;
	  stop_sound ();
	}
      break;

    case (5):
      if (play_c_scale () < 0)
	{
	  btn_nr = -1;
	  stop_sound ();
	}
      break;

    case (6):
      break;

    case (7):
      break;
    }



}

/* GPIO even pin interrupt handler */
void __attribute__ ((interrupt)) GPIO_EVEN_IRQHandler ()
{
  *GPIO_IFC = *GPIO_IF;		//Clear interrupt
  GPIO_handler ();
}

/* GPIO odd pin interrupt handler */
void __attribute__ ((interrupt)) GPIO_ODD_IRQHandler ()
{
  *GPIO_IFC = *GPIO_IF;		//Clear interrupt
  GPIO_handler ();
}

/* handler to combine GPIO_EVEN and GPIO_ODD IRQ_Handlers */
void
GPIO_handler ()
{
  uint32_t btn_input;
  btn_input = gpio_read_buttons ();	//gets active high values

  //static uint32_t last_input;
  uint32_t pressed = last_input ^ btn_input;
  pressed = pressed & btn_input;
  last_input = btn_input;

  k = 0;

  switch (pressed)
    {
    case (1 << 0):		//SW1
      //*GPIO_PA_DOUT = ~((1 << 0) << 8);
      setupDAC ();
      setupTimer (SAMPLE_PERIOD);
      startTimer ();
      btn_nr = 1;

      break;

    case (1 << 1):		//SW2
      //*GPIO_PA_DOUT = ~((1 << 1) << 8);
      setupDAC ();
      setupTimer (SAMPLE_PERIOD);
      startTimer ();
      btn_nr = 2;
      break;

    case (1 << 2):		//SW3
      //*GPIO_PA_DOUT = ~((1 << 2) << 8);
      setupDAC ();
      setupTimer (SAMPLE_PERIOD);
      startTimer ();
      btn_nr = 3;

      break;
    case (1 << 3):		//SW4
      //*GPIO_PA_DOUT = ~((1 << 3) << 8);
      setupDAC ();
      setupTimer (SAMPLE_PERIOD);
      startTimer ();
      btn_nr = 4;
      break;

    case (1 << 4):		//SW5
      //*GPIO_PA_DOUT = ~((1 << 4) << 8);
      setupDAC ();
      setupTimer (SAMPLE_PERIOD);
      startTimer ();
      btn_nr = 5;
      break;

    case (1 << 5):		//SW6
      //*GPIO_PA_DOUT = ~((1 << 5) << 8);
      stop_sound ();
      break;

    case (1 << 6):		//SW7
      //*GPIO_PA_DOUT = ~((1 << 6) << 8);
      stop_sound ();
      break;

    case (1 << 7):		//SW8
      //*GPIO_PA_DOUT = ~((1 << 7) << 8);
      stop_sound ();
      break;


    }
}
