#ifndef NOTES_H
#define NOTES_H

#include <stdint.h>


typedef struct note_t
{

  int num_samples;
  uint16_t samples[];

} note_t;


note_t *get_C6 (void);
note_t *get_D6 (void);
note_t *get_E6 (void);
note_t *get_F6 (void);
note_t *get_G6 (void);
note_t *get_A6 (void);
note_t *get_B6 (void);
note_t *get_C7 (void);


#endif
