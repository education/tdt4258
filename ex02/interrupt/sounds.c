#include <stdint.h>
#include <stdbool.h>
#include <stdint.h>

#include "sounds.h"

#include "efm32gg.h"
#include "dac.h"
#include "gpio.h"
#include "timer.h"
#include "dac.h"

static int k;


static int j = 0;
note_t *temp_note;
int temp_time;
int temp_length;


int
play_note (note_t * note, int time_ms)
{

  if (j <= ((int) SAMP_PR_MS * time_ms))	//integer is time in ms
    {
      writeDAC (note->samples[j % note->num_samples]);
      j++;
      return 0;			// not finished with note samples
    }
  else
    {

      return 1;			//finished with note samples
    }

}

int
play_sound3 ()
{

  note_t *sound3[] = {
    get_C6 (), get_D6 (), get_E6 (), get_F6 (), get_G6 (), get_F6 (),
    get_E6 (), get_D6 (), get_C6 (),
    get_C6 (), get_D6 (), get_E6 (), get_F6 (), get_G6 (), get_F6 (),
    get_E6 (), get_D6 (), get_C6 ()
  };

  int sound3_time[] =
    { R, R, R, R, R, R, R, R, R, R, R, R, R, R, R, R, R, R };

  temp_note = sound3[k];
  temp_time = sound3_time[k];

  if (1 == play_note (temp_note, temp_time))
    {
      j = 0;
      k++;
    }
  if (k > 18)
    {
      k = 0;
      return -1;
    }
  return 1;
}

int
play_sound1 ()
{

  note_t *sound1[] =
    { get_C6 (), get_D6 (), get_E6 (), get_F6 (), get_E6 (), get_D6 (),
    get_C6 ()
  };
  int sound1_time[] = { R_2, R_2, R_2, R_2, R_2, R_2, R_2 };

  temp_note = sound1[k];
  temp_time = sound1_time[k];

  if (1 == play_note (temp_note, temp_time))
    {
      j = 0;
      k++;
    }
  if (k > 7)
    {
      k = 0;
      return -1;
    }
  return 1;
}

int
play_c_scale ()
{

  note_t *c_scale[] =
    { get_C6 (), get_D6 (), get_E6 (), get_F6 (), get_G6 (), get_A6 (),
    get_B6 (), get_C7 ()
  };
  int c_scale_time[] = { R_2, R_2, R_2, R_2, R_2, R_2, R_2, R_2 };

  temp_note = c_scale[k];
  temp_time = c_scale_time[k];

  if (1 == play_note (temp_note, temp_time))
    {
      j = 0;
      k++;
    }
  if (k > 8)
    {
      k = 0;
      return -1;
    }
  return 1;
}

int
play_lisa ()
{
  note_t *lisa[] = {
    get_C6 (), get_D6 (), get_E6 (), get_F6 (),
    get_G6 (), get_G6 (),
    get_A6 (), get_A6 (), get_A6 (), get_A6 (),
    get_G6 (),
    get_F6 (), get_F6 (), get_F6 (), get_F6 (),
    get_E6 (), get_E6 (),
    get_D6 (), get_D6 (), get_D6 (), get_D6 (),
    get_C6 ()
  };
  int lisa_time[] = {
    R, R, R, R,
    R_2, R_2,
    R, R, R, R,
    R_4,
    R, R, R, R,
    R_2, R_2,
    R, R, R, R,
    R_4
  };

  temp_note = lisa[k];
  temp_time = lisa_time[k];

  if (1 == play_note (temp_note, temp_time))
    {
      j = 0;
      k++;

    }
  if (k > 22)
    {
      k = 0;
      return -1;
    }
  return 1;
}


void
stop_sound ()
{

  //stopTimer ();
  disableTimer ();
  DAC_off ();

}
