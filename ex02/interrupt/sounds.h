#ifndef SOUNDS_H
#define SOUNDS_H


#include "notes.h"


#define CORECLK     16000000
#define SAMP_PR_MS 44.1
#define SAMPLE_PERIOD 317	// 44100 samp/ sek  period = time pr. interrupt * (f_clk / prescaler)

#define R 100
#define R_2 2*R
#define R_4 4*R

/* play samples in a note_t struct 
*
* note_t* note takes in the pointer to note struct
* int time_ms decides how long to play the note
*
* return 1 if finished with current note
* return 0 if not finished with current note
*/
int play_note (note_t * note, int time_ms);

/* prototypes for play functions */
int play_c_scale ();
int play_lisa ();
int play_sound1 ();
int play_sound2 ();
int play_sound3 ();


/* Disables the Timer and DAC */
void stop_sound ();



#endif
