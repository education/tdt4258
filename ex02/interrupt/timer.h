#ifndef TIMER_H
#define TIMER_H


/* setup timer with a period of period

*	period =  sample_rate / core_clk
*
*/
void setupTimer (uint32_t period);

void disableTimer (void);

void startTimer (void);

void stopTimer (void);


#endif
