#include <stdint.h>
#include <stdbool.h>
#include <stdint.h>

#include "efm32gg.h"
#include "dac.h"
#include "gpio.h"
#include "notes.h"

#define TENTH_OF_SECOND 14000

char *notes1 = "BY}6YB6%";
char *notes2 = "Qj}6jQ6%";

/* create note based on arbitrary arithemtic on amplitude */
uint16_t
getNote (int volume, int mask, int octave, int instrument)
{
  uint16_t selection = 3 & volume >> 16;
  char *selected = selection ? notes1 : notes2;
  uint16_t baseNote = selected[octave % 8] + 51;
  uint16_t note = (baseNote * volume) >> instrument;
  uint16_t result = 3 & mask & note;
  return (result << 4);
}


void
play_sound (uint16_t lyd)
{
  uint16_t temp = lyd;
  int length;
  switch (temp)
    {
    case (0):

      length = 30;

      for (int i = 0; i < TENTH_OF_SECOND * length; i++)
	{
	  writeDAC (SINE_WAVE[(i / 2) % 255]);
	}

      break;

    case (1):

      length = 30;

      for (int i = 0; i < TENTH_OF_SECOND * length; i++)
	{
	  writeDAC (TRIANGLE_WAVE[i % 255]);
	}
      break;


    case (2):

      length = 200;

      for (int i = 0; i < TENTH_OF_SECOND * length; i++)
	{
	  writeDAC (i >> 2);
	}

      break;

    case (3):

      length = 200;

      for (int i = 0; i < TENTH_OF_SECOND * length; i++)
	{
	  uint16_t n = i >> 21;	//14
	  uint16_t s = i >> 18;	//17
	  uint16_t ins1 = getNote (i, 1, n, 12);
	  uint16_t ins2 = getNote (i, s, n ^ i >> 13, 10);
	  uint16_t ins3 = getNote (i, s / 3, n + ((i >> 11) % 3), 10);
	  uint16_t ins4 = getNote (i, s / 5, 8 + n - ((i >> 10) % 3), 9);
	  uint16_t combined = ins1 + ins2 + ins3 + ins4;
	  writeDAC (combined);
	}


      break;


    }
}

void
manage_sound (uint16_t btn_input)
{
  static uint32_t last_input;

  uint32_t pressed = last_input ^ btn_input;
  pressed = pressed & btn_input;
  last_input = btn_input;

  switch (pressed)
    {
    case (1 << 0):
      play_sound (0);
      break;
    case (1 << 1):
      play_sound (1);
      break;
    case (1 << 2):
      play_sound (2);
      break;
    case (1 << 3):
      play_sound (3);
      break;
    }
}
