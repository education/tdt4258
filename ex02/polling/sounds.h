#ifndef SOUNDS_H
#define SOUNDS_H


/* writes new values to DAC each clock cycle 
*
* input lyd  is an integer choosing what sound to be played
*/
void play_sound (uint16_t lyd);

/* reads button input, and controll button functions
*
* input btn_input takes in high bit for the button pressed 
*/
void manage_sound (uint16_t btn_input);


#endif
