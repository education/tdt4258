#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>	// interface for allocation of memory regions
#include <linux/fs.h>		// filesystem header, required for writing device drivers
#include <asm/io.h>		// accessing I/O memory
#include <linux/kdev_t.h>	// minor and major number macros
#include <linux/cdev.h>		//char device registration
#include <asm/uaccess.h>	// driver to access user space
#include <linux/interrupt.h>

#include <linux/device.h>
#include <linux/moduleparam.h>
#include <asm/signal.h>
#include <asm/siginfo.h>

#include <linux/module.h>	// macro defining THIS_MODULE
#include "efm32gg.h"

#define GAMEPAD_DRIVER_NAME "gamepad_driver"

#define GPIO_EVEN_IRQ_LINE 17
#define GPIO_ODD_IRQ_LINE 18

//variables
unsigned int firstminor = 0;
unsigned int count = 1;		// total number of continous major device numbers to be requested
dev_t devno = 0;		// holds first number in allocated major number range
dev_t device_nr_minor = 0;	//usual default value
struct cdev gamepad_driver_cdev;

struct fasync_struct *fasync_queue;

struct class *cl;		//struct containing device class
dev_t devno;

/* function prototypes */
static int __init template_init(void);
static int __exit template_exit(void);
static irqreturn_t gpio_interrupt_handler(int irq, void *dev_id,
					  struct pt_regs *regs);
static int gamepad_driver_fasync(int, struct file *, int mode);
static int gamepad_driver_open(struct inode *inode, struct file *filp);
static int gamepad_driver_release(struct inode *inode, struct file *filp);
static ssize_t gamepad_driver_read(struct file *filp, char __user * buff,
				   size_t count, loff_t * offp);
static ssize_t gamepad_driver_write(struct file *filp, char *__user buff,
				    size_t count, loff_t * offp);

static struct file_operations gamepad_driver_fops = {
	.owner = THIS_MODULE,
	.open = gamepad_driver_open,
	.release = gamepad_driver_release,
	.read = gamepad_driver_read,
	.write = gamepad_driver_write,
	.fasync = gamepad_driver_fasync,
};

static irqreturn_t gpio_interrupt_handler(int irq, void *dev_id,
					  struct pt_regs *regs)
{

	iowrite32(ioread32(GPIO_IF), GPIO_IFC);	// clear interrupt flag

	if (fasync_queue) {
		kill_fasync(&fasync_queue, SIGIO, POLL_IN);	//used to signal user-space process when data arrives
	}

	return IRQ_HANDLED;
}

/*
 * gamepad_init - function to insert this module into kernel space
 *
 * This is the first of two exported functions to handle inserting this
 * code into a running kernel
 *
 * Returns 0 if successfull, otherwise -1
 */

static int __init gamepad_init(void)
{
	printk(KERN_INFO "Hello World, here is your module speaking\n");

	// Dynamically allocate device number
	if (alloc_chrdev_region(&devno, 0, count, GAMEPAD_DRIVER_NAME)) {
		//enters if, if the allocation fails
		printk(KERN_INFO "Allocating driver number failed \n");
		return -1;	//failed
	}
	printk(KERN_INFO "Device major number is: %d \n", devno);

	// Allocate memory for I/O registers in port C, used by gamepad
	// allocates a memory region of 1 byte, from GPIO_PC_MODEL
	// returns non-NULL on sucsess
	if (request_mem_region(GPIO_PC_MODEL, 1, GAMEPAD_DRIVER_NAME) == NULL) {
		printk(KERN_INFO "Failed to allocate GPIO_PC_MODEL \n");
	}

	if (request_mem_region(GPIO_PC_DOUT, 1, GAMEPAD_DRIVER_NAME) == NULL) {
		printk(KERN_INFO "Failed to allocat e GPIO_PC_DOUT \n");
	}
	if (request_mem_region(GPIO_PC_DIN, 1, GAMEPAD_DRIVER_NAME) == NULL) {
		printk(KERN_INFO "Failed to allocate GPIO_PC_DIN \n");
	}
	// Memory map the I/O memory
	// ioremap_nocache(unsigned long phys_addr, unsigned long size);
	void *gpio_pc_model = ioremap_nocache(GPIO_PC_MODEL, 1);
	void *gpio_pc_dout = ioremap_nocache(GPIO_PC_DOUT, 1);
	void *gpio_exitsell = ioremap_nocache(GPIO_EXTIPSELL, 1);

	void *gpio_extifall = ioremap_nocache(GPIO_EXTIFALL, 1);
	void *gpio_gpio_ien = ioremap_nocache(GPIO_IEN, 1);
	void *gpio_ifc = ioremap_nocache(GPIO_IFC, 1);

	// Init GPIO 
	// shoud be preformed with a base adress obtained from the ioremap
	iowrite32(0x33333333, gpio_pc_model);	// set pins 0-7 to input
	iowrite32(0xFF, gpio_pc_dout);	// enable internal pull-up
	iowrite32(0x22222222, gpio_exitsell);	// select interrupt

	//Init interrupts
	if (request_irq
	    (GPIO_EVEN_IRQ_LINE, (irq_handler_t) gpio_interrupt_handler, 0,
	     GAMEPAD_DRIVER_NAME, &gamepad_driver_cdev) < 0) {
		printk(KERN_INFO
		       "request_irq even failed, cant get assigned to irq %i \n",
		       GPIO_EVEN_IRQ_LINE);
	}

	if (request_irq
	    (GPIO_ODD_IRQ_LINE, (irq_handler_t) gpio_interrupt_handler, 0,
	     GAMEPAD_DRIVER_NAME, &gamepad_driver_cdev) < 0) {
		printk(KERN_INFO
		       "request_irq odd failed, cant get assigned to irq %i \n",
		       GPIO_ODD_IRQ_LINE);
	}

	//add device
	cdev_init(&gamepad_driver_cdev, &gamepad_driver_fops);
	gamepad_driver_cdev.owner = THIS_MODULE;

	// create device class of device (neded to make the file appear in /dev)
	cl = class_create(THIS_MODULE, GAMEPAD_DRIVER_NAME);
	device_create(cl, NULL, devno, NULL, GAMEPAD_DRIVER_NAME);
	cdev_add(&gamepad_driver_cdev, devno, count);

	printk(KERN_INFO "__init function complete \n");

	return 0;
}

/*
 * gamepad_exit - function to cleanup this module from kernel space
 *
 * This is the second of two exported functions to handle cleanup this
 * code from a running kernel
 */
static void __exit gamepad_exit(void)
{
	printk("__exit start \n");
	printk("Short life for a small module...\n");

	// Free device number
	unregister_chrdev_region(devno, count);	//unregister first number, count is continous uumbers after to remove

	// Free I/O memory
	release_mem_region(GPIO_PC_MODEL, 1);
	release_mem_region(GPIO_PC_DOUT, 1);
	release_mem_region(GPIO_PC_DIN, 1);

	//Disable interrupts
	iowrite32(0x0000, GPIO_IEN);

	//Free interrupts
	free_irq(GPIO_EVEN_IRQ_LINE, &gamepad_driver_cdev);	//check value
	free_irq(GPIO_ODD_IRQ_LINE, &gamepad_driver_cdev);	//check value

	//release device
	device_destroy(cl, devno);	//check
	class_destroy(cl);	// 
	cdev_del(&gamepad_driver_cdev);	// remove char device from the system

	printk(" __exit complete \n");

}

module_init(gamepad_init);	// specifies which function will be used as init
module_exit(gamepad_exit);	// the same, but for exit funciton

MODULE_DESCRIPTION("Small module, demo only, not very useful.");
MODULE_LICENSE("GPL");		//specifies the license for the code 

/* 
*	user program opens the driver
*	always the first operation performed on the device file.
* 	function not required, but without it we cant get notified if the function sucseeds
*/
static int gamepad_driver_open(struct inode *inode, struct file *filp)
{

	// Enable interrupts
	iowrite32(0xFF, GPIO_EXTIFALL);	//
	iowrite32(0x00FF, GPIO_IEN);	// enable interruts on
	iowrite32(0xFF, GPIO_IFC);	//clear interupt flag register

	printk(KERN_INFO "Driver opened\n");
	return 0;		//sucsess

}

/*
* 	user program closes the driver
* 	invoked when the file structure is being released
*/
static int gamepad_driver_release(struct inode *inode, struct file *filp)
{
	printk(KERN_INFO "Driver : Driver released\n");

	//remove the file from the list of active asynchronous readers
	gamepad_driver_fasync(-1, filp, 0);

	return 0;
}

/* 
*	user program reads from the driver
*	reads button status from GPIO_PC_DIN
*/
static ssize_t gamepad_driver_read(struct file *filp, char __user * buff,
				   size_t count, loff_t * offp)
{
	uint32_t btn_status = ioread32(GPIO_PC_DIN);
	copy_to_user(buff, &btn_status, 1);	//copy to buff, from btn_status, 1 byte length
	return 1;		//return the numer of bytes sucsessfully read
}

/* 
*	user program writes to the driver
*	not needed for the gamepad
*/
static ssize_t gamepad_driver_write(struct file *filp, char __user * buff,
				    size_t count, loff_t * offp)
{
	return 1;		//number of bytes sucsessfylly written to device
}

/*
* 	fasync function
*	Notifies the device of a change in its FASYNC flag
*/

static int gamepad_driver_fasync(int fd, struct file *filp, int mode)
{
	// add or remove entries from the list of interested processes when the FASYNC
	//      flag changes for an open file
	return fasync_helper(fd, filp, mode, &fasync_queue);

}
