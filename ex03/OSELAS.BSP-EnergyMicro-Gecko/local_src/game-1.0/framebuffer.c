#include "framebuffer.h"

//const size_t mapPixelSize = 320 * 240;
const size_t mapSizeByte = sizeof(uint16_t) * 320 * 240;

int initFrameBuffer()
{

	int fd;

	printf("Opening framebufferFile\n");

	fd = open("/dev/fb0", O_RDWR);
	if (fd == -1) {
		printf("Error: unable to open framebuffer device.\n");
		return EXIT_FAILURE;
	}

	printf("Framebuffer initialized\n");

	return fd;
}

uint16_t *setUpMemoryMap(int fileDescriptor)
{

	uint16_t *mapbuffer;

	mapbuffer =
	    (uint16_t *) mmap(0, mapSizeByte, (PROT_READ | PROT_WRITE),
			      MAP_SHARED, fileDescriptor, 0);

	return mapbuffer;
}

void closeMemoryMap(uint16_t * mapbuffer)
{
	munmap(mapbuffer, mapSizeByte);
}

/*
void disco()
{

	uint16_t *writeBuffer;

	size_t writecount = 640 * 240;

	writeBuffer = malloc(sizeof(size_t) * writecount);

	int i;

	for (i = 0; i < writecount; ++i) {
		writeBuffer[i] = 0b1111100000000000;
	}

	write(framebufferDescriptor, writeBuffer, writecount);

	for (i = 0; i < writecount; ++i) {
		writeBuffer[i] = 0b0000011111100000;
	}

	sleep(2);

	write(framebufferDescriptor, writeBuffer, writecount);

	for (i = 0; i < writecount; ++i) {
		writeBuffer[i] = 0 b0000000000011111;
	}

	sleep(2);

	write(framebufferDescriptor, writeBuffer, writecount);

	free(writeBuffer);
}
*/
