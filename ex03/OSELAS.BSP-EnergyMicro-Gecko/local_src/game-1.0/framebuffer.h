#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

//Includes for file opening and closing
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//For mmap
#include <sys/mman.h>
#include <linux/fb.h>
#include <sys/ioctl.h>

//Initialization of frame buffer

uint16_t *setUpMemoryMap(int fileDescriptor);
void closeMemoryMap(uint16_t * mapbuffer);
int initFrameBuffer();

#endif
