#include "game.h"
#include "framebuffer.h"
#include "snake.h"

FILE *device;
uint8_t last_input;

int input = 0;

//2 Frames per second
double MS_PER_FRAME = 500;	//4 Frames per second
double start;

//Draw game over to the sceen
void gameOver()
{
	uint16_t color = 0b1111100000000000;
	uint16_t letterStartx = 0;
	uint16_t letterStarty = 0;
	int teller = 0;
	int i = 0;

	int G[8][25] = {
		{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0},	//G
		{1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0},	//A
		{1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},	//M
		{0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1},	//E

		{1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0},	//O
		{1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0},	//V
		{0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1},	//E
		{0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1}	//R

	};

	if (game_over_reset) {

		for (teller = 0; teller < 8; teller++) {

			if (teller == 4) {

				letterStartx = 0;
				letterStarty = letterStarty + 10;
			}

			for (uint16_t y = letterStarty; y < (letterStarty + 5);
			     y++) {

				for (uint16_t x = letterStartx;
				     x < (letterStartx + 5); x++) {

					int drawBit = G[0][i];

					if (drawBit == 1) {
						drawSquare(x, y, color,
							   framebufferMem);
					}
					i++;
				}

			}
			letterStartx = letterStartx + 5;

		}
		game_over_reset = 0;
	}
}

// Driver functions
int get_btn_nr(int input)
{
	input = ~input;		// invert input, to get active high button input

	for (int i = 0; i < 8; i++) {
		int tmp = input & (1 << i);	//reads each button bit, stor high value in tmp
		int expected_input = (1 << i);	//expected value for butten nr i
		if (tmp == expected_input)
			return (i + 1);	// +1 to make first button btn1
	}
	return 0;

}

//Interupts from the game-pad runs this function, and update a variable containing information about the latest input
void sigio_handler()
{

	int button_input = get_btn_nr(fgetc(device));	//reads character values form device driver

	switch (button_input) {
	case 5:
		last_input = 4;	//left
		break;
	case 6:
		last_input = 1;	// up
		break;
	case 7:
		last_input = 2;	// right
		break;

	case 8:
		last_input = 3;	//down
		break;
	}
}

int init_gamepad()
{
	//open driver file
	device = fopen("/dev/gamepad_driver", "rb");
	//sets the disposition of the SIGIO to the sigio_handler
	signal(SIGIO, &sigio_handler);

	// writes the game procces id into F_SETOWN                             
	fcntl(fileno(device), F_SETOWN, getpid());

	// return the file acces mode and the file (devise driver file) status flag (fasync flag)
	long fasync_flag = fcntl(fileno(device), F_GETFL);

	// sets the file status flag, if fasync_flag or FASYNC is high.
	// this enables notification from the driver 
	fcntl(fileno(device), F_SETFL, fasync_flag | FASYNC);

	return EXIT_SUCCESS;
}

int main()
{
	// Turn off cursor_blink in framebuffer
	system("echo 0 > /sys/class/graphics/fbcon/cursor_blink");

	//Init of gamepad drive
	system("modprobe driver-gamepad");

	if (init_gamepad() == EXIT_FAILURE) {
		printf("Error: unable to init gamepad.\n");
		return EXIT_FAILURE;
	}
	//Init of framebuffer
	framebufferDescriptor = initFrameBuffer();
	framebufferMem = setUpMemoryMap(framebufferDescriptor);

	//Init of game state
	if (initGame()) {
		printf("Something went wrong when initializing the game\n");
	}

	printf("The game was initialized\n");
	last_input = 4;

	//Game loop

	while (1) {

		start = walltime();	//When did we start the current iteration of the game loop;

		//Update the game model based on the last input from the gamepad and render the game
		updateGameModel(last_input);
		render();
		double end = walltime();
		//Sleep untill next frame

		//Check if the game should be stopped because of colision with snake body.
		while (stop_game) {
			//Display game over screen
			gameOver();
			//When a button is pressed restart the game by initializing the game state again
			if (get_btn_nr(fgetc(device)) != 0) {
				if (initGame()) {
					printf
					    ("Something went wrong when initializing the game\n");
				}

				printf("The game was initialized\n");
				last_input = 4;
				stop_game = 0;
			}

		}
		printf("game loop execution time = %f ms \n",
		       ((end - start) * 1000));

		usleep(((start + MS_PER_FRAME) - walltime()) * 1000);	//Multiplying by 1k to convert from ms to microsec

	}

	exit(EXIT_SUCCESS);
}
