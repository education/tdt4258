
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 1
#define _GNU_SOURCE 1
#endif

#ifndef GAME_H
#define GAME_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <errno.h>

//#######################################
//Input handeling

int get_btn_nr(int input);
int init_gamepad();
void sigio_handler();

//#######################################
//Initialization
int init();

#endif
