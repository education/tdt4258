#include "snake.h"

const size_t mapPixelWidth = 320;
const size_t mapPixelHeight = 240;

const size_t squareSize = 16;

const int size = 20 * 15;

// Map dimensions
const int mapwidth = 20;
const int mapheight = 15;

double MS_PER_FRAME;

typedef enum e_direction { UP = 1, RIGHT, DOWN, LEFT } Direction;
Direction snake_direction;

void drawSquare(uint16_t xpos, uint16_t ypos, uint16_t color, uint16_t * buffer)
{

	uint16_t xstartpixel = xpos * squareSize;
	uint16_t ystartpixel = ypos * squareSize;

	uint16_t i, j;
	for (i = xstartpixel; i < xstartpixel + squareSize; ++i) {
		for (j = ystartpixel; j < ystartpixel + squareSize; ++j) {
			buffer[i + j * mapPixelWidth] = color;
		}
	}

	struct fb_copyarea rect;

	rect.dx = xstartpixel;
	rect.dy = ystartpixel;
	rect.width = squareSize;
	rect.height = squareSize;

	ioctl(framebufferDescriptor, 0x4680, &rect);

}

void drawBackground(uint16_t x, uint16_t y)
{
	uint16_t color = 0b1111111111111111;
	drawSquare(x, y, color, framebufferMem);
}

void drawSnake(uint16_t x, uint16_t y)
{
	uint16_t color = 0b0000000000011111;
	drawSquare(x, y, color, framebufferMem);
}

void drawFood(uint16_t x, uint16_t y)
{
	uint16_t color = 0b1111100000011111;
	drawSquare(x, y, color, framebufferMem);
}

/* Timing */
double walltime()
{
	static struct timeval t;
	gettimeofday(&t, NULL);
	return (t.tv_sec + 1e-6 * t.tv_usec);
}

void changeDirection(int snake_direction)
{
	/*
	   W
	   A   +   D
	   S

	   1
	   4   +   2
	   3
	 */

	switch (snake_direction) {
	case UP:
		if (direction != 3) {
			direction = 1;
		}
		break;
	case RIGHT:
		if (direction != 4) {
			direction = 2;
		}
		break;
	case DOWN:
		if (direction != 1) {
			direction = 3;
		}
		break;
	case LEFT:
		if (direction != 2) {
			direction = 4;
		}
		break;
	default:
		break;
	}

}				//Move the snake

void moveSnake(int dx, int dy)
{
	// Determine new head position
	int newx = (headxpos + dx);
	int newy = (headypos + dy);

	if (newx < 0) {
		newx = 19;
	} else if (newx > 19) {
		newx = 0;
	}

	if (newy < 0) {
		newy = 14;
	} else if (newy > 14) {
		newy = 0;
	}
	// Check if there is food at location
	if (newx == foodxpos && newy == foodypos) {
		// Increase food value (body length)
		food++;
		MS_PER_FRAME = MS_PER_FRAME / 1.1;
		// Generate new food on map
		generateFood();
	}
	// Check location is free
	else if (map[mapIndex(newx, newy)] != 0) {
		headxpos = newx;
		headypos = newy;
		stop_game = 1;
		game_over_reset = 1;

	}
	// Move head to new location
	headxpos = newx;
	headypos = newy;

	map[mapIndex(headxpos, headypos)] = food + 1;
}

void updateMap()
{

	switch (direction) {
	case 1:
		moveSnake(0, -1);
		break;
	case 2:
		moveSnake(1, 0);
		break;
	case 3:
		moveSnake(0, 1);
		break;
	case 4:
		moveSnake(-1, 0);
		break;
	}
	// Reduce snake values on map by 1
	for (uint16_t y = 0; y < mapheight; y++) {
		for (uint16_t x = 0; x < mapwidth; x++) {
			if (map[mapIndex(x, y)] > 0) {
				map[mapIndex(x, y)]--;
				if (map[mapIndex(x, y)] == 0) {
					drawBackground(x, y);
				}
			}
		}
	}
}

void updateGameModel(int input)
{
	changeDirection(input);
	updateMap();
}

void render()
{
	//For each block in the map check if the snakes is there and draw to the screen.
	for (uint16_t y = 0; y < mapheight; y++) {
		for (uint16_t x = 0; x < mapwidth; x++) {

			if (map[mapIndex(x, y)] == 0
			    && (x != foodxpos || y != foodypos)) {
				//drawBackground(x, y); // updated background. Commented out improvers energy preformance. The entire screen is not updated each frame, only the snake.
			} else if (map[mapIndex(x, y)] != 0
				   && (x != foodxpos || y != foodypos)) {
				drawSnake(x, y);
			}
		}
	}
	drawFood(foodxpos, foodypos);
}

// mapIndex takes in the x and y coordinate of the snake, and returns the right index to the map
int mapIndex(int xpos, int ypos)
{
	return xpos + (ypos * mapwidth);
}

void generateFood()
{
	do {
		// Generate random x and y values within the map
		foodxpos = rand() % mapwidth;
		foodypos = rand() % mapheight;
		// If location is not free try again
	} while (map[mapIndex(foodxpos, foodypos)] != 0);

	map[mapIndex(foodxpos, foodypos)] = size + 1;
}

int initGame()
{

	stop_game = 0;
	snake_direction = UP;
	//Set the position of the snake to the middle of the screen
	MS_PER_FRAME = 500;

	headxpos = mapwidth / 2;
	headypos = mapheight / 2;

	//The snake should be heading to the south
	direction = 4;
	//Set the lenght of the snake
	food = 3;

	//Allocate memory for the map
	map = (int *)calloc(size, sizeof(int));

	//Set the values in the map where the snake is supposed to start
	//The head starts at position (headxpos, headypos)
	map[mapIndex(headxpos, headypos)] = 3;
	map[mapIndex(headxpos, headypos) + 1] = 2;
	map[mapIndex(headxpos, headypos) + 2] = 1;

	running = true;

	//generate random values, srand starts initializing random values. 
	srand(time(NULL));

	generateFood();

	for (uint16_t y = 0; y < mapheight; y++) {
		for (uint16_t x = 0; x < mapwidth; x++) {

			if ((map[mapIndex(x, y)] == 0)
			    && (x != foodxpos || y != foodypos)) {
				drawBackground(x, y);
			}
		}
	}

	render();

	return 0;
}
