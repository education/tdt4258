#ifndef SNAKE_H
#define SNAKE_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

//For timing the loop
#include <sys/time.h>
#include <time.h>

//Includes for file opening and closing
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//For mmap
#include <sys/mman.h>
#include <linux/fb.h>
#include <sys/ioctl.h>

// Snake head details
int headxpos;
int headypos;
int direction;

// Amount of food the snake has (How long the body is)
int food;

int foodxpos;
int foodypos;

// The tile values for the map
int *map;

// Determine if game is running
bool running;
int stop_game;

// Makes sure Game Over only is written once
int game_over_reset;

int framebufferDescriptor;

//Framebuffer memory
uint16_t *framebufferMem;

void drawSquare(uint16_t xpos, uint16_t ypos, uint16_t color,
		uint16_t * buffer);

//#######################################
//Helping functions
int mapIndex(int xpos, int ypos);

void drawBackground(uint16_t x, uint16_t y);
void drawSnake(uint16_t x, uint16_t y);
void drawFood(uint16_t x, uint16_t y);
void render();

//#######################################
//Game state

void changeDirection(int input);
void moveSnake(int dx, int dy);
void updateMap();
void updateGameModel(int input);
void generateFood();

double walltime();

int initGame();

#endif
